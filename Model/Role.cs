﻿using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace LabWork1_5.Model
{
    public class Role : INotifyPropertyChanged
    {
        public int Id { get; set; }
        private string roleName;

        public Role() { }
        public Role(int id, string roleName)
        {
            this.Id = id;
            this.RoleName = roleName;
        }
        public string RoleName
        {
            get
            {
                return roleName;
            }

            set
            {
                roleName = value;
                OnPropertyChanged("RoleName");
            }
        }

        public Role ShallowCopy()
        {
            return (Role)this.MemberwiseClone();
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
