﻿using LabWork1_5.Helper;
using LabWork1_5.ViewModel;
using System;

namespace LabWork1_5.Model
{
    public class Person
    {
        public int Id { get; set; }
        public int RoleId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime Birthday { get; set; }

        public Person() { }

        public Person(int id, int roleId, string firstName, string lastName, DateTime birthday)
        {
            this.Id = id;
            this.RoleId = roleId;
            this.FirstName = firstName;
            this.LastName = lastName;
            this.Birthday = birthday;
        }

        public Person CopyFromPersonDPO(PersonDPO p)
        {
            RoleViewModel vmRole = new RoleViewModel();
            int roleId = 0;

            foreach (var r in vmRole.ListRole)
            {
                if (r.RoleName == p.RoleName)
                {
                    roleId = r.Id;
                    break;
                }
            }

            if (roleId != 0)
            {
                this.Id = p.Id;
                this.RoleId = roleId;
                this.FirstName = p.FirstName;
                this.LastName = p.LastName;
                this.Birthday = p.Birthday;
            }
            return this;
        }
    }
}