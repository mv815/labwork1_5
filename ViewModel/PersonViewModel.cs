﻿using LabWork1_5.Helper;
using LabWork1_5.Model;
using LabWork1_5.View;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows;

namespace LabWork1_5.ViewModel
{
    class PersonViewModel : INotifyPropertyChanged
    {
        private PersonDPO selectedPersonDpo;
        private RelayCommand addPerson;
        private RelayCommand editPerson;
        private RelayCommand deletePerson;
        private List<Role> RoleList;
        private RoleViewModel vmRole = new RoleViewModel();

        public PersonDPO SelectedPersonDpo
        {
            get { return selectedPersonDpo; }
            set
            {
                selectedPersonDpo = value;
                OnPropertyChanged("SelectedPersonDpo");
            }
        }
        public ObservableCollection<Person> ListPerson { get; set; } = new ObservableCollection<Person>();
        public ObservableCollection<PersonDPO> ListPersonDpo { get; set; } = new ObservableCollection<PersonDPO>();
        public PersonViewModel()
        {
            this.ListPerson.Add(
                new Person
                {
                    Id = 1,
                    RoleId = 1,
                    FirstName = "Иван",
                    LastName = "Иванов",
                    Birthday = new DateTime(1980, 02, 28)
                });

            this.ListPerson.Add(
                new Person
                {
                    Id = 2,
                    RoleId = 2,
                    FirstName = "Петр",
                    LastName = "Петров",
                    Birthday = new DateTime(1981, 03, 20)
                });

            this.ListPerson.Add(
                new Person
                {
                    Id = 3,
                    RoleId = 3,
                    FirstName = "Виктор",
                    LastName = "Викторов",
                    Birthday = new DateTime(1961, 08, 11)
                });

            this.ListPerson.Add(
                new Person
                {
                    Id = 4,
                    RoleId = 3,
                    FirstName = "Сидор",
                    LastName = "Сидоров",
                    Birthday = new DateTime(1955, 02, 20)
                });
            ListPersonDpo = GetListPersonDpo();
            RoleList = vmRole.ListRole.ToList();
        }
        public ObservableCollection<PersonDPO> GetListPersonDpo()
        {
            foreach (var person in ListPerson)
            {
                PersonDPO p = new PersonDPO();
                p = p.CopyFromPerson(person);
                ListPersonDpo.Add(p);
            }
            return ListPersonDpo;
        }

        public int MaxId()
        {
            int max = 0;

            foreach (var r in this.ListPerson)
            {
                if (max < r.Id) max = r.Id;
            }
            return max;
        }

        public RelayCommand AddPerson
        {
            get
            {
                return addPerson ??
                (addPerson = new RelayCommand(obj =>
                {
                    WindowNewEmployee wnPerson = new WindowNewEmployee
                    {
                        Title = "Новый сотрудник",
                    };

                    int maxIdPerson = MaxId() + 1;
                    PersonDPO per = new PersonDPO
                    {
                        Id = maxIdPerson,
                        Birthday = DateTime.Now
                    };
                    wnPerson.DataContext = per;
                    wnPerson.CbRole.ItemsSource = RoleList;
                    wnPerson.CbRole.SelectedIndex = 0;

                    if (wnPerson.ShowDialog() == true)
                    {
                        Role r = (Role)wnPerson.CbRole.SelectedValue;
                        per.RoleName = r.RoleName;
                        ListPersonDpo.Add(per);
                        Person p = new Person();
                        p = p.CopyFromPersonDPO(per);
                        ListPerson.Add(p);
                    }
                },
                (obj) => true));
            }
        }
        public RelayCommand EditPerson
        {
            get
            {
                return editPerson ??
                (editPerson = new RelayCommand(obj =>
                {
                    WindowNewEmployee wnPerson = new WindowNewEmployee()
                    {
                        Title = "Редактирование данных сотрудника"
                    };

                    Person tempPerson = new Person();
                    PersonDPO personDpo = SelectedPersonDpo;
                    PersonDPO tempPersonDpo = new PersonDPO();
                    int selectedRoleId = tempPerson.CopyFromPersonDPO(personDpo).RoleId - 1;

                    tempPersonDpo = personDpo.ShallowCopy();
                    wnPerson.DataContext = tempPersonDpo;
                    wnPerson.CbRole.ItemsSource = RoleList;
                    wnPerson.CbRole.SelectedIndex = selectedRoleId;
                    if (wnPerson.ShowDialog() == true)
                    {
                        Role r = (Role)wnPerson.CbRole.SelectedValue;
                        personDpo.RoleName = r.RoleName;
                        personDpo.FirstName = tempPersonDpo.FirstName;
                        personDpo.LastName = tempPersonDpo.LastName;
                        personDpo.Birthday = tempPersonDpo.Birthday;
                        FindPerson finder = new FindPerson(personDpo.Id);
                        List<Person> listPerson = ListPerson.ToList();
                        Person p = listPerson.Find(new Predicate<Person>(finder.PersonPredicate));
                        p = p.CopyFromPersonDPO(personDpo);
                    }
                }, (obj) => SelectedPersonDpo != null && ListPersonDpo.Count > 0));
            }
        }
        public RelayCommand DeletePerson
        {
            get
            {
                return deletePerson ??
                (deletePerson = new RelayCommand(obj =>
                {
                    PersonDPO person = SelectedPersonDpo; 
                    MessageBoxResult result = MessageBox.Show("Удалить данные по сотруднику: \n" + person.LastName + " " + person.FirstName, "Предупреждение", 
                        MessageBoxButton.OKCancel, 
                        MessageBoxImage.Warning);
                    if (result == MessageBoxResult.OK)
                    {
                        ListPersonDpo.Remove(person);
                        Person per = new Person();
                        per = per.CopyFromPersonDPO(person);
                        ListPerson.Remove(per);
                    }
                }, (obj) => SelectedPersonDpo != null && ListPersonDpo.Count > 0));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
