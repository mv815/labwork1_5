﻿using LabWork1_5.Helper;
using LabWork1_5.Model;
using LabWork1_5.View;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows;

namespace LabWork1_5.ViewModel
{
    class RoleViewModel : INotifyPropertyChanged
    {
        private Role selectedRole;
        private RelayCommand addRole;
        private RelayCommand editRole;
        private RelayCommand deleteRole;
        public ObservableCollection<Role> ListRole { get; set; } = new ObservableCollection<Role>();
        public RoleViewModel()
        {
            this.ListRole.Add(
                new Role
                {
                    Id = 1,
                    RoleName = "Директор"
                });

            this.ListRole.Add(
                new Role
                {
                    Id = 2,
                    RoleName = "Бухгалтер"
                });

            this.ListRole.Add(
                new Role
                {
                    Id = 3,
                    RoleName = "Менеджер"
                });
        }
        public Role SelectedRole
        {
            get { return selectedRole; }
            set
            {
                selectedRole = value;
                OnPropertyChanged("SelectedRole");
                EditRole.CanExecute(true);
            }
        }
        public RelayCommand AddRole
        {
            get
            {
                return addRole ??
                    (addRole = new RelayCommand(obj =>
                    {
                        WindowNewRole wnRole = new WindowNewRole
                        {
                            Title = "Новая должность"
                        };
                        int maxIdRole = MaxId() + 1;
                        Role role = new Role { Id = maxIdRole };
                        wnRole.DataContext = role;
                        if (wnRole.ShowDialog() == true)
                        {
                            ListRole.Add(role);
                        }
                    }));
            }
        }
        public RelayCommand EditRole
        {
            get
            {
                return editRole ??
                    (editRole = new RelayCommand(obj =>
                    {
                        WindowNewRole wnRole = new WindowNewRole
                        {
                            Title = "Редактирование должности"
                        };
                        Role role = SelectedRole;
                        Role tempRole = new Role();
                        tempRole = role.ShallowCopy();
                        wnRole.DataContext = tempRole;
                        if (wnRole.ShowDialog() == true)
                        {
                            role.RoleName = tempRole.RoleName;
                        }
                    }, (obj) => SelectedRole != null && ListRole.Count > 0));
            }
        }
        public RelayCommand DeleteRole
        {
            get
            {
                return deleteRole ??
                (deleteRole = new RelayCommand(obj =>
                {
                    Role role = SelectedRole;
                    MessageBoxResult result = MessageBox.Show("Удалить данные по должности: " + role.RoleName, 
                        "Предупреждение", 
                        MessageBoxButton.OKCancel, 
                        MessageBoxImage.Warning);
                    if (result == MessageBoxResult.OK)
                    {
                        ListRole.Remove(role);
                    }
                }, (obj) => SelectedRole != null && ListRole.Count > 0));
            }
        }
        public int MaxId()
        {
            int max = 0;
            foreach (var r in this.ListRole)
            {
                if (max < r.Id) max = r.Id;
            }
            return max;
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
