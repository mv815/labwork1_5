﻿using LabWork1_5.Model;

namespace LabWork1_5.Helper
{
    class FindPerson
    {
        int id;
        public FindPerson(int id)
        {
            this.id = id;
        }

        public bool PersonPredicate(Person person)
        {
            return person.Id == id;
        }
    }
}
