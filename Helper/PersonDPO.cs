﻿using LabWork1_5.Model;
using LabWork1_5.ViewModel;
using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace LabWork1_5.Helper
{
    public class PersonDPO : INotifyPropertyChanged
    {
        public int Id { get; set; }
        private string roleName;
        private string firstName;
        private string lastName;
        private DateTime birthday;

        public string RoleName 
        {
            get { return roleName; }
            set
            {
                roleName = value;
                OnPropertyChanged("RoleName");
            }
        }
        public string FirstName 
        { 
            get { return firstName; }  
            set 
            { 
                firstName = value;
                OnPropertyChanged("FirstName");
            }
        }
        public string LastName 
        { 
            get { return lastName; } 
            set
            {
                lastName = value;
                OnPropertyChanged("LastName");
            }
        }
        public DateTime Birthday 
        { 
            get { return birthday; } 
            set 
            {
                birthday = value;
                OnPropertyChanged("Birthday");
            }
        }
        public PersonDPO() { }
        public PersonDPO(int id, string roleName, string firstName, string lastName, DateTime birthday)
        {
            this.Id = id;
            this.RoleName = roleName;
            this.FirstName = firstName;
            this.LastName = lastName;
            this.Birthday = birthday;
        }
        public PersonDPO CopyFromPerson(Person person)
        {
            PersonDPO perDPO = new PersonDPO();
            RoleViewModel vmRole = new RoleViewModel();
            string role = string.Empty;

            foreach (var r in vmRole.ListRole)
            {
                if (r.Id == person.RoleId)
                {
                    role = r.RoleName;
                    break;
                }
            }

            if (role != string.Empty)
            {
                perDPO.Id = person.Id;
                perDPO.RoleName = role;
                perDPO.FirstName = person.FirstName;
                perDPO.LastName = person.LastName;
                perDPO.Birthday = person.Birthday;
            }
            return perDPO;
        }

        public PersonDPO ShallowCopy()
        {
            return (PersonDPO)this.MemberwiseClone();
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}